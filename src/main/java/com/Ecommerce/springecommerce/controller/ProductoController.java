package com.Ecommerce.springecommerce.controller;

import com.Ecommerce.springecommerce.model.Producto;
import com.Ecommerce.springecommerce.model.Usuario;
import com.Ecommerce.springecommerce.service.ProductoService;
import org.slf4j.LoggerFactory;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/productos")
public class ProductoController {

    private final Logger LOGGER= LoggerFactory.getLogger(ProductoController.class);
    @Autowired
    private ProductoService productoService;


    @GetMapping("")
    public String show(){
        return "administrador/productos/show";
    }


    @GetMapping("/create")
    public String create(){
        return "administrador/productos/create";
    }
    @PostMapping("/save")
    public String save(Producto producto){
        LOGGER.info("Este es el objeto producto {}",producto);
        Usuario u= new Usuario(1,"","","","","","","");
        producto.setUsuario(u);
        productoService.save(producto);
        return "redirect:/productos";
    }


}
