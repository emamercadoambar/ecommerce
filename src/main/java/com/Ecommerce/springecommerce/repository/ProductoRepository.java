package com.Ecommerce.springecommerce.repository;

import com.Ecommerce.springecommerce.model.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface ProductoRepository extends JpaRepository<Producto,Integer> {




}
