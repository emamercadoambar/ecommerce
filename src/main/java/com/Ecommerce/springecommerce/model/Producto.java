package com.Ecommerce.springecommerce.model;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@Entity
@Table(name="productos")
public class Producto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nombre;
    private  String descripcion;
    private String imagen;
    private double precio;
    private int cantidad;

    //muchos a uno
    @ManyToOne
    private Usuario usuario;



}
